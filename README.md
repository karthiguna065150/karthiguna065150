[![Visitors](https://hits.seeyoufarm.com/api/count/incr/badge.svg?url=https%3A%2F%2Fgitlab.com%2Fkarthiguna065150%2F&count_bg=%2379C83D&title_bg=%23555555&icon=nvidia.svg&icon_color=%23E7E7E7&title=Visitors&edge_flat=false)](https://hits.seeyoufarm.com)


<div align="center">
    <h1 align="center">
        <img src="https://readme-typing-svg.herokuapp.com/?font=Righteous&size=35&center=true&vCenter=true&width=700&height=100&duration=1000&lines=Hi+There!+👋;+I'm+Guna! 👨‍💻;" />
    </h1>
    <h3 align="center">A passionate DevOps Engineer from India 🇮🇳</h3>
</div>

<br/>

<div align="center">
 
 🔭 I’m currently working on **Cloud agnostic Microservice build and Deployment using CI CD**
 
 🌱 I’m currently learning **Terraform, Ansible, RedHat Linux**

💬 Ask me about **Kubernetes, Docker, CI CD, Python... or anything**


</div>

<div align="center"> 
  <a href="mailto:karthiguna065150@gmail.com">
    <img src="https://img.shields.io/badge/Gmail-333333?style=for-the-badge&logo=gmail&logoColor=red" />
  </a>
  <a href="https://www.linkedin.com/in/gunasekar-velraj-a82499138/" target="_blank">
    <img src="https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white" target="_blank" />
  </a>
</div>
<hr/>
<h2 align="center">⚒️ Skills ⚒️</h2>
<br/>
<div align="center">
    <img src="https://skillicons.dev/icons?i=kubernetes,docker,python,aws,gcp,azure,linux,bash,redhat,terraform,git,gitlab,flask,ansible,vscode,github" /><br>
</div>




