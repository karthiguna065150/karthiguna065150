FROM python:alpine

COPY . .

RUN pip install flask

CMD python gitlab_status_fetcher/app.py