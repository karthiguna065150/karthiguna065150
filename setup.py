from setuptools import setup 
import json

setup( 
	name='gitlab_stats_fetcher', 
	version='0.0.1', 
	description='Api to fetch gitlab activity status like commits and MRs', 
	author='Gunasekar Velraj', 
	author_email='karthiguna065150@gmail.com', 
	packages=['gitlab_stats_fetcher'], 
	install_requires=["flask"]
) 
