from flask import Flask, jsonify, request
import requests
import os

app = Flask(__name__)



@app.route("/")
def home():
    return {"status": "healthy"}

GITLAB_API_TOKEN = os.getenv("GITLAB_TOKEN")
GITLAB_USERNAME = 'karthiguna065150'

headers = {
    'Private-Token': GITLAB_API_TOKEN
}

def fetch_events(user_id):
    events = []
    page = 1
    while True:
        response = requests.get(f'https://gitlab.com/api/v4/users/{user_id}/events', headers=headers, params={'page': page, 'per_page': 100})
        page_events = response.json()
        if not page_events:
            break
        events.extend(page_events)
        page += 1
    return events

def calculate_activity(events, activity_type):
    if activity_type == 'commits':
        count = sum(1 for event in events if event['action_name'] == 'pushed to')
    elif activity_type == 'merge_requests':
        count = sum(1 for event in events if event['action_name'] == 'accepted')
    elif activity_type == 'issues':
        count = sum(1 for event in events if event['action_name'] == 'opened')
    else:
        count = 0
    return count

@app.route('/activity')
def get_activity():
    activity_type = request.args.get('type')
    response = requests.get(f'https://gitlab.com/api/v4/users?username={GITLAB_USERNAME}', headers=headers)
    user_data = response.json()[0]
    events = fetch_events(user_data['id'])
    count = calculate_activity(events, activity_type)
    
    return jsonify({
        'schemaVersion': 1,
        'label': activity_type,
        'message': str(count),
        'color': 'blue' if activity_type == 'commits' else 'green' if activity_type == 'merge_requests' else 'red'
    })

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
